# Info

Setup is for managing everything about a Unix setup. This can include:
+ Dotfiles (symlinking like stow)
+ Initial package installation

# Usage

## Dotfiles
All dotfiles should be placed in the `dotfile` directory. Within the dotfile directory there should be a folder for each set of dotfiles you want symlinked.

If you want these symlinked to any other location besides home then you can specify in a `config.yaml` file located in each folder where they are linked to.

Example `config.yaml`
``` yaml
link-dir: /home/username/example-dir/
```

## Installation
All installation targets should be contained in their own directories. Each directory (name doesn't matter) should contain a `config.yaml`. 

These config files should contain instructions for how to install the given target.

Example `*.yaml` for packages
``` yaml
install-type: package-manager

install-command: xbps-install 
uninstall-command: xbps-remove

packages:
    - sdl2
    - libsqlite
    - other-package
    - other-package2
```

Example `*.yaml` for builds
``` yaml
install-type: build

install-commands:
    - git clone package-link
    - git clone patch-files
    - patch blah blah
    - echo "exec /dwm/loc" > ~/.xinitrc

clean-commands:
    - cleanup build remnants, etc.

uninstall-commands:
    - do removal stuff, etc.
```

## Usage

`setup [setup-directory] [install,uninstall,link,unlink,check] [target]`

### install `[optional-name]`
This argument runs through the install functions
If the optional name is supplied it only installs that package.

### uninstall `[optional-name]`
This argument runs through uninstall functions located within each install `*.yaml`.
If the optional name is supplied it only uninstalls that package.

### unlink `package-name`
This command unlinks the associated symlinks with `package-name`.

### link `package-name`
Creates the symlinks associated with the given package

### check `target`
Checks whether or not the given target has a proper folder structure.
