use serde_derive::Deserialize;
use std::path::PathBuf;
use std::collections::HashMap;
use log::{error, trace, warn};

#[derive(Deserialize, Debug)]
pub struct Mapping {
    pub from: String,
    pub to:   String
}

///Maps the name of a directory to a list of mappings as well as
///files/directories within that mapping.
pub type MappingResult =
    HashMap<String, (Vec<Mapping>, Vec<std::path::PathBuf>)>;

pub fn find_dots(dot_folder_path: &std::path::PathBuf) -> anyhow::Result<MappingResult> {
    let mut ret = HashMap::new();

    let dot_folders = dot_folder_path.read_dir()?;
    for dir in dot_folders {
        let dir = (dir?).path();
        let dot_files = (dir.read_dir()?).into_iter();

        let mut add = Vec::new();
        for dot_dir in dot_files {
            let path = (dot_dir?).path();

            //lol
            let name = path
                .file_name()
                .unwrap()
                .to_str()
                .unwrap();

            if name != "mapping.yaml" {
                add.push(path);
            }
        }

        let filename = dir.file_name()
            .ok_or(anyhow::anyhow!("invalid filename '{}'", dir.to_str().unwrap()))?
            .to_str()
            .unwrap();

        let mut config_file = dir.clone();
        config_file.push("mapping.yaml");

        let mapping;
        if config_file.is_file() {
            let config = std::fs::File::open(config_file.clone())?;

            mapping = match serde_yaml::from_reader(config) {
                Ok(mapping) => mapping,
                Err(err)    => anyhow::bail!(
                    "malformed '{}', {}",
                    config_file.to_str().unwrap(),
                    err.to_string()
                ),
            };
        }else{
            mapping = Vec::new();
        }

        ret.insert(filename.to_string(), (mapping, add));
    }

    Ok(ret)
}

#[inline]
pub fn strip_home(path: String) -> PathBuf {
    if path.starts_with("~") {
        let home = dirs::home_dir()
            .expect("unable to retrieve home directory");

        let conv = home
            .as_os_str()
            .to_string_lossy();
        let path_str = path.replacen("~", &conv, 1);
        PathBuf::from(path_str)
    }else{
        PathBuf::from(path)
    }
}

pub fn is_symlink(link: &PathBuf) -> bool {
    let link_path = link.as_path();
    link_path.symlink_metadata()
        .map(|x| x.file_type().is_symlink())
        .unwrap_or(false)
}

///Checks whether or not a link points to the given file.
///Returns an error if the given file was not a symlink.
fn points_at_me(link: &PathBuf, dest: &PathBuf) -> anyhow::Result<bool> {
    if is_symlink(link) {
        let res = std::fs::read_link(link.as_path()).unwrap();
        Ok(*dest == res)
    }else{
        Ok(false)
    }
}


///Recurisvely links a directory and it's contents into another directory.
///Ex: mapping a folder to ~/.config will first attempt to create a symlink to ~/.config.
///When it founds that cannot be done, it recursively expands the folder and symlinks the
///contents until it is able to create a symlink. It then stops expanding that tree and
///moves to the next.
fn recursive_link(dest: &mut PathBuf, from: &mut PathBuf, make: bool) -> anyhow::Result<Vec<Mapping>> {
    let mut ret = Vec::new();

    if dest.exists() {
        if (!dest.is_dir() && !dest.is_file()) || is_symlink(dest) {
            match points_at_me(&dest, &from) {
                Ok(check) => {
                    if check {
                        let to   = dest.to_str().unwrap().to_owned();
                        let from = from.to_str().unwrap().to_owned();

                        if make {
                            trace!("Link from '{}' to '{}' already exists", to, from);
                        }

                        ret.push(Mapping {
                            to,
                            from
                        });

                        Ok(ret)
                    }else{
                        anyhow::bail!(
                            "failed to link '{}', destination '{}' is a symlink to somewhere else.",
                            from.to_str().unwrap(),
                            dest.to_str().unwrap()
                        );
                    }
                },
                Err(_) => {
                    anyhow::bail!(
                        "failed to link '{}', destination '{}' exists and is not a directory",
                        from.to_str().unwrap(),
                        dest.to_str().unwrap()
                    );
                }
            }
        }else{
            if !from.is_dir() {
                anyhow::bail!(
                    "failed to link '{}' to '{}' destination exists, and the root is not a directory",
                    from.to_str().unwrap(),
                    dest.to_str().unwrap()
                );
            }else{
                let interior = from.read_dir()?;
                for interior_file in interior {
                    let interior_file = interior_file?;
                    let file_name = interior_file
                        .file_name()
                        .clone();

                    dest.push(file_name);
                    let res = recursive_link(dest, &mut (interior_file.path()), make);
                    dest.pop();

                    match res {
                        Ok(mut res) => ret.append(&mut res),
                        Err(err) => warn!("{}", err.to_string())
                    }
                }

                Ok(ret)
            }
        }
    }else{
        use std::os::unix::fs;

        let to   = dest.to_str().unwrap().to_owned();
        let from = from.to_str().unwrap().to_owned();

        if !make {
            ret.push(Mapping {
                to,
                from
            });
        }else{
            match fs::symlink(from.clone(), dest.clone()) {
                Ok(_) => {
                    trace!("Created link '{}' to '{}'", to, from);

                    ret.push(Mapping {
                        to,
                        from
                    });
                },
                Err(err) => {
                    error!(
                        "failed to link '{}' to '{}': {}",
                        from,
                        to,
                        err.to_string()
                    );
                }
            }
        }

        Ok(ret)
    }
}

///Used to create or check whether or not a symlink can be made.
///This way during the check phase, you can determine whether or not
///your mappings are valid without actually making them.
pub fn link(root_path: &PathBuf, mapping: &Mapping, make: bool) -> anyhow::Result<Vec<Mapping>> {
    let mut to   = strip_home(mapping.to.clone());
    let mut from = root_path.join(strip_home(mapping.from.clone()));

    if !to.is_absolute() {
        anyhow::bail!("cannot map to non-absolute filepath");
    }else if !from.exists() {
        anyhow::bail!(format!("'{}' does not exist", from.to_str().unwrap()));
    }else {
        recursive_link(&mut to, &mut from, make)
    }
}
