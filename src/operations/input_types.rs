pub enum DeleteResponse {
    All,
    None,
    Some,
    Print
}

pub enum DeleteResponseErr {
    Question,
    Other(String)
}

impl core::str::FromStr for DeleteResponse {
    type Err = DeleteResponseErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let trimmed = s.trim().to_ascii_lowercase();

        match trimmed.as_str() {
            "all"   | "a"   => Ok(DeleteResponse::All),
            "none"  | "n"  => Ok(DeleteResponse::None),
            "some"  | "s"  => Ok(DeleteResponse::Some),
            "print" | "p" => Ok(DeleteResponse::Print),
            "?"     => Err(DeleteResponseErr::Question),
            _       => Err(DeleteResponseErr::Other(s.to_string()))
        }
    }
}

pub enum YesNoResponse {
    Yes,
    No
}

impl core::str::FromStr for YesNoResponse {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let trimmed = s.trim().to_ascii_lowercase();

        match trimmed.as_str() {
            "yes" | "y" => Ok(YesNoResponse::Yes),
            "no"  | "n" => Ok(YesNoResponse::No),
            _           => Err(())
        }
    }
}
