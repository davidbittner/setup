mod input_types;

use self::input_types::*;

use crate::dots;
use read_input::prelude::*;

use log::{error, info, warn, trace};

use std::path::PathBuf;

fn find_dot_dirs(path: &std::path::Path) -> anyhow::Result<dots::MappingResult> {
    match dots::find_dots(&path.to_path_buf()) {
        Ok(dirs) => Ok(dirs),
        Err(err) => 
            Err(anyhow::anyhow!("occured while parsing dotfiles '{}'", err.to_string()))
    }
}

pub fn check(path: &std::path::Path) {
    let dirs = match find_dot_dirs(path) {
        Ok(dirs) => dirs,
        Err(err) => {
            error!("{}", err.to_string());
            return;
        }
    };

    for (dir_name, (maps, list)) in dirs {
        info!(
            "Registered dots '{}' with '{}' sub-directories. Found '{}' mappings",
            dir_name, list.len(), maps.len()
        );

        let dir_path = path.join(dir_name);
        for map in maps.into_iter() {
            let link = dots::link(&dir_path, &map, false);
            match link {
                Ok(_)   => (),
                Err(err) => {
                    error!(
                        "Invalid mapping '{}' to '{}': {}",
                        map.from,
                        map.to,
                        err.to_string()
                    );
                }
            }
        }
    }

    info!("Done.");
}

pub fn link(path: &std::path::Path, passed_dirs: Vec<String>) {
    let dirs = match find_dot_dirs(path) {
        Ok(dirs) => dirs,
        Err(err) => {
            error!("{}", err.to_string());
            return;
        }
    };

    for (dir_name, (maps, mut list)) in dirs {
        if !passed_dirs.contains(&dir_name) && !passed_dirs.is_empty() {
            continue;
        }

        info!(
            "Found dots '{}' with '{}' associated files. Found '{}' mappings",
            dir_name, list.len(), maps.len()
        );

        let dir_path = path.join(dir_name.clone());

        //These are the files/directories that did not have a mapping
        //They get mapped to ~/.config/folder_name
        list.retain(|x| {
            for map in maps.iter() {
                let temp_path = dir_path.join(std::path::PathBuf::from(&map.from));
                if temp_path == *x {
                    return false;
                }
            }
            true
        });

        let mut dir_links = Vec::new();
        for unmapped in list.into_iter() {
            let mut to = dots::strip_home("~/.config".to_string());
            to.push(unmapped.file_name().unwrap());

            let mut from = dir_path.clone();
            from.push(&(unmapped.file_name().unwrap()));

            let temp_mapping = dots::Mapping{
                to:   to.to_str().unwrap().to_owned(),
                from: from.to_str().unwrap().to_owned()
            };

            let link = dots::link(&dir_path, &temp_mapping, true);
            match link {
                Ok(mut links_made) => {
                    dir_links.append(&mut links_made);
                },
                Err(err) => {
                    warn!(
                        "invalid mapping '{}' to '{}': {}",
                        temp_mapping.from,
                        temp_mapping.to,
                        err.to_string()
                    );
                }
            }
        }

        for map in maps.into_iter() {
            let link = dots::link(&dir_path, &map, true);
            match link {
                Ok(mut links_made) => {
                    dir_links.append(&mut links_made);
                },
                Err(err) => {
                    warn!(
                        "invalid mapping '{}' to '{}': {}",
                        map.from,
                        map.to,
                        err.to_string()
                    );
                }
            }
        }

        info!(
            "{} link(s) made/already exists for '{}'.",
            dir_links.len(),
            dir_name
        );
    }
}

fn delete_symlink(file: String) {
    let temp_buf = PathBuf::from(&file);
    if crate::dots::is_symlink(&temp_buf) {
        //Respond to every error minus a NotFound error
        //Not found errors are very common if files were added
        //to the dotfile structure but never linked
        let res = std::fs::remove_file(&file);
        match res {
            Ok(_) => trace!("Deleted link '{}'.", file),
            Err(err) => {

                match err.kind() {
                    std::io::ErrorKind::NotFound => (),
                    _ => warn!("Error occurred while deleting '{}'. '{}'", file, err.to_string()),
                }
            }

        }
    }else if temp_buf.exists() {
        warn!("File '{}' found instead of symlink. Maybe you created it? Not deleting.", file);
    }
}

pub fn unlink(path: &std::path::Path, passed_dirs: Vec<String>) {
    let dirs = match find_dot_dirs(path) {
        Ok(dirs) => dirs,
        Err(err) => {
            error!("{}", err.to_string());
            return;
        }
    };

    for (dir_name, (maps, mut paths)) in dirs {
        if !passed_dirs.contains(&dir_name) && !passed_dirs.is_empty() {
            continue;
        }

        let dir_path = path.join(dir_name.clone());

        let mut deletes = Vec::new();

        //These are the files/directories that did not have a mapping
        //They get mapped to ~/.config/folder_name
        paths.retain(|x| {
            for map in maps.iter() {
                let temp_path = dir_path.join(std::path::PathBuf::from(&map.from));
                if temp_path == *x {
                    return false;
                }
            }
            true
        });

        for unmapped in paths {
            let mut dest_path = PathBuf::from(dots::strip_home("~/.config".to_string()));
            dest_path.push(unmapped.file_name().unwrap());

            let temp_mapping = dots::Mapping {
                from: unmapped.to_str().unwrap().to_owned(),
                to:   dest_path.to_str().unwrap().to_owned()
            };

            match dots::link(&dir_path, &temp_mapping, false) {
                Ok(links_made) => {
                    for link in links_made.into_iter() {
                        if PathBuf::from(&link.to).exists() {
                            deletes.push(link.to);
                        }
                    }
                },
                Err(err) => {
                    warn!("Unable to generate unmapped symlink paths: '{}'", err.to_string());
                }
            }
        }

        for map in maps.into_iter() {
            //don't actually make symlinks, just generate a list of ones that would be made
            let link = dots::link(&dir_path, &map, false);
            match link {
                Ok(links_made) => {
                    for link in links_made.into_iter() {
                        if PathBuf::from(&link.to).exists() {
                            deletes.push(link.to);
                        }
                    }
                },
                Err(err) => {
                    warn!(
                        "Invalid mapping '{}' to '{}': {}. Check your config.",
                        map.from,
                        map.to,
                        err.to_string()
                    );
                }
            }
        }

        if deletes.len() == 0 {
            info!("No links exist for '{}', skipping.", dir_name);
            continue;
        }

        let msg_str = format!(
            "{} symlink(s) were found for '{}'. (S)ome/(N)one/(A)ll/(P)rint/(?): ",
            deletes.len(),
            dir_name
        );

        loop {
            let response = input::<DeleteResponse>()
                .repeat_msg(&msg_str)
                .default(DeleteResponse::All)
                .err_match(|err| {
                    Some(match err {
                        DeleteResponseErr::Question => {
                            let msg = "
'Some' will walk you through a dialog allowing you to choose which files get deleted.
'None' will just continue without deleting any files.
'All' will delete every file without prompt.
'Print' will simply print all the files that will be deleted.

No files that are not symlinks will be deleted. Further, it will warn you if it finds a file in a place it expected a symlink.
                            ";

                            format!("{}", msg)
                        },
                        DeleteResponseErr::Other(resp) => {
                            format!("{} was not understood.", resp)
                        }
                    })
                }).get();

            match response {
                DeleteResponse::All => {
                    info!("Deleting {} links.", deletes.len());

                    for file in deletes.into_iter() {
                        delete_symlink(file);
                    }

                    break;
                }
                DeleteResponse::Some => {
                    for file in deletes.into_iter() {
                        let response = input::<YesNoResponse>()
                            .repeat_msg(format!("Delete file '{}'? Y/n: ", file))
                            .default(YesNoResponse::Yes)
                            .err("Input not understood.")
                            .get();

                        match response {
                            YesNoResponse::Yes => {
                                delete_symlink(file);
                            },
                            YesNoResponse::No  => ()
                        }
                    }
                    break;
                },
                DeleteResponse::Print => {
                    for file in deletes.iter() {
                        info!("{}", file);
                    }
                },
                DeleteResponse::None => break
            }
        }
    }
}
