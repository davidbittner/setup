mod dots;
mod operations;

use clap::{Arg, App, SubCommand, AppSettings};
use std::path::{Path, PathBuf};
use log::error;

fn generate_target(target: &Path) -> std::io::Result<std::path::PathBuf> {
    let mut ret = PathBuf::from(target);

    if !ret.is_absolute() {
        let join = match std::env::current_dir() {
            Ok(path) => path,
            Err(err) => panic!("Unable to get current directory: '{}'", err.to_string())
        };

        ret = join.join(ret);
    }

    ret.canonicalize()
}

fn main() -> std::io::Result<()> {
    use env_logger::{Builder, Env, fmt::Color};
    use std::io::Write;

    human_panic::setup_panic!();

    Builder::from_env(Env::default().default_filter_or("info"))
        .format(|buf, record| {
            let mut level_style = buf.style();

            level_style.set_bold(true);

            use log::Level;
            match record.level() {
                Level::Error => level_style.set_color(Color::Red),
                Level::Warn  => level_style.set_color(Color::Yellow),
                Level::Info  => level_style.set_color(Color::White),
                Level::Trace => level_style.set_color(Color::Green),
                Level::Debug => level_style.set_color(Color::Blue),
            };

            writeln!(buf, "{}--> {}", level_style.value(record.level()), record.args())
        })
        .init();

    let matches = 
        App::new("SetUp")
        .setting(AppSettings::SubcommandRequired)
        .version("261218")
        .author("David Bittner <bittneradave@gmail.com>")
        .about("Used to manage dotfiles as well as basic setup related builds")
        .subcommand(SubCommand::with_name("check")
            .about("Checks the given directory's folder structure")
            .arg(Arg::with_name("PATH")
                .help("The directory containing your folder structure")
                .required(true)
                .index(1)))
        .subcommand(SubCommand::with_name("link")
            .about("SymLinks a given folder using it's mapping file")
            .arg(Arg::with_name("PATH")
                 .help("The directory containing your folder structure")
                 .required(true)
                 .index(1))
            .arg(Arg::with_name("FOLDER")
                 .help("The name of the folder within the dotfiles directory")
                 .multiple(true)
                 .index(2)))
        .subcommand(SubCommand::with_name("unlink")
            .about("Removes all of the symlinks previously created for a given folder.")
            .arg(Arg::with_name("PATH")
                 .help("The directory containing your folder structure")
                 .required(true)
                 .index(1))
            .arg(Arg::with_name("FOLDER")
                 .help("The name of the folder within the dotfiles directory")
                 .multiple(true)
                 .index(2))).get_matches();

    if let Some(args) = matches.subcommand_matches("check") {
        let path = std::path::Path::new(args.value_of("PATH").unwrap());
        operations::check(path);
    }else if let Some(args) = matches.subcommand_matches("link") {
        let path = std::path::Path::new(args.value_of("PATH").unwrap());

        let val: Vec<String>;
        if args.is_present("FOLDER") {
            val = args
                .values_of("FOLDER")
                .unwrap()
                .map(|x| x.to_owned())
                .collect();
        }else{
            val = Vec::new();
        }

        let target = generate_target(path);
        if let Ok(target) = target {
            if !target.exists() {
                error!("Directory '{}' does not exist.", target.display());
            }else{
                operations::link(&target, val.to_owned());
            }
        }else{
            let err = target.unwrap_err();
            error!("Unable to normalize given path to TARGET '{}': '{}'", path.display(), err.to_string());
        }
    }else if let Some(args) = matches.subcommand_matches("unlink") {
        let path = std::path::Path::new(args.value_of("PATH").unwrap());
        
        let val: Vec<String>;
        if args.is_present("FOLDER") {
            val = args
                .values_of("FOLDER")
                .unwrap()
                .map(|x| x.to_owned())
                .collect();
        }else{
            val = Vec::new();
        }

        let target = generate_target(path);
        if let Ok(target) = target {
            if !target.exists() {
                error!("Directory '{}' does not exist.", target.display());
            }else{
                operations::unlink(&target, val.to_owned());
            }
        }else{
            let err = target.unwrap_err();
            error!("Unable to normalize given path to TARGET '{}': '{}'", path.display(), err.to_string());
        }
    }

    Ok(())
}
